/* eslint-disable react/no-unused-prop-types */
/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { setLocale } from '@containers/App/actions';
import AppBar from '@mui/material/AppBar';
import Avatar from '@mui/material/Avatar';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Menu, MenuItem, IconButton } from '@mui/material';
import FlagId from '@static/images/flags/id.png';
import FlagEn from '@static/images/flags/en.png';
import PropTypes from 'prop-types';
import classes from './style.module.scss';

const Index = ({ locale }) => {
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [menuPosition, setMenuPosition] = useState(null);
  const open = Boolean(menuPosition);

  const handleBurgerClick = () => {
    setMobileMenuOpen(!mobileMenuOpen);
  };

  const handleClick = (event) => {
    setMenuPosition(event.currentTarget);
  };

  const handleClose = () => {
    setMenuPosition(null);
  };

  const onSelectLang = (lang) => {
    if (lang !== locale) {
      dispatch(setLocale(lang));
    }
    handleClose();
  };

  return (
    <header className={classes.header}>
      <div className={classes.left}>
        <div className={classes.logo}>Shortly</div>
        {/* Hide the desktop menu on mobile devices */}
        <ul className={`${classes.navlinks} ${mobileMenuOpen ? classes.open : ''}`}>
          <li>
            <FormattedMessage id="app_features" />
          </li>
          <li>
            <FormattedMessage id="app_pricing" />
          </li>
          <li>
            <FormattedMessage id="app_resources" />
          </li>
        </ul>
      </div>
      <div className={classes.right}>
        <div className={classes.login}>
          <FormattedMessage id="app_login" />
        </div>
        <div className={`${classes.signup} ${mobileMenuOpen ? classes.hide : ''}`}>
          <FormattedMessage id="app_signup" />
        </div>
        {/* Burger icon */}
        <div className={classes.translate}>
          <div className={classes.burger} onClick={handleBurgerClick}>
            <Menu open={open} fontSize="large">
              <div className={`${classes.bar} ${mobileMenuOpen ? classes.rotateTop : ''}`} />
              <div className={`${classes.bar} ${mobileMenuOpen ? classes.hideBar : ''}`} />
              <div className={`${classes.bar} ${mobileMenuOpen ? classes.rotateBottom : ''}`} />
            </Menu>
          </div>

          <Menu open={open} anchorEl={menuPosition} onClose={handleClose}>
            <MenuItem onClick={() => onSelectLang('id')} selected={locale === 'id'}>
              <div className={classes.menu}>
                <Avatar className={classes.menuAvatar} src={FlagId} />
                <div className={classes.menuLang}>
                  <FormattedMessage id="app_lang_id" />
                </div>
              </div>
            </MenuItem>
            <MenuItem onClick={() => onSelectLang('en')} selected={locale === 'en'}>
              <div className={classes.menu}>
                <Avatar className={classes.menuAvatar} src={FlagEn} />
                <div className={classes.menuLang}>
                  <FormattedMessage id="app_lang_en" />
                </div>
              </div>
            </MenuItem>
          </Menu>
          <div className={classes.toggle} onClick={handleClick}>
            <Avatar className={classes.avatar} src={locale === 'id' ? FlagId : FlagEn} />
            <div className={classes.lang}>{locale}</div>
            <ExpandMoreIcon />
          </div>
        </div>
      </div>
      {/* Mobile menu */}
      <div className={`${classes.mobileMenu} ${mobileMenuOpen ? classes.showMenu : ''}`}>
        <ul>
          <li>
            <FormattedMessage id="app_features" />
          </li>
          <li>
            <FormattedMessage id="app_pricing" />
          </li>
          <li>
            <FormattedMessage id="app_resources" />
          </li>
        </ul>
        <div className={classes.login}>
          <FormattedMessage id="app_login" />
        </div>
        <div className={classes.signup}>
          <FormattedMessage id="app_signup" />
        </div>
      </div>
    </header>
  );
};

Index.propTypes = {
  locale: PropTypes.string.isRequired,
};

export default Index;
