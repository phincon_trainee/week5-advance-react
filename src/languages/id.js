export default {
  app_greeting: 'Hai dari Web!',
  app_title_header: 'Vite + React',
  app_not_found: 'Halaman tidak ditemukan',
  app_lang_id: 'Bahasa Indonesia',
  app_lang_en: 'Bahasa Inggris',

  // header
  app_features: 'Fitur',
  app_pricing: 'Harga',
  app_resources: 'Sumber Daya',
  app_login: 'Masuk',
  app_signup: 'Daftar',

  // Page1
  app_More: 'Lebih dari sekadar tautan pendek',
  app_Build: 'Bangun pengenalan merek Anda dan dapatkan wawasan terperinci tentang performa tautan Anda.',
  app_started: 'Mulai Sekarang',

  // Shortener
  app_inputLink: 'Pendekkan tautan di sini...',
  app_shorten: 'Pendekkan!',
  app_clear: 'Hapus',
  app_clearAll: 'Hapus Semua',
  app_copy: 'Salin',
  app_copied: 'Tersalin',

  // Cards
  app_cardsTitle: 'Statistik Lanjutan',
  app_cardsChildText: 'Lacak performa tautan Anda di seluruh web dengan dasbor statistik lanjutan kami.',
  app_cardsTitle1: 'Pengenalan Merek',
  app_cardsChildText1:
    'Tingkatkan pengenalan merek Anda dengan setiap klik. Tautan generik tidak memiliki arti. Tautan bermerk membantu menanamkan kepercayaan dalam konten Anda.',
  app_cardsTitle2: 'Catatan Detail',
  app_cardsChildText2:
    'Dapatkan wawasan tentang siapa yang mengklik tautan Anda. Mengetahui kapan dan di mana orang berinteraksi dengan konten Anda membantu menginformasikan keputusan yang lebih baik.',
  app_cardsTitle3: 'Sepenuhnya Dapat Dikustomisasi',
  app_cardsChildText3:
    'Tingkatkan kesadaran merek dan penemuan konten melalui tautan yang dapat disesuaikan, meningkatkan keterlibatan audiens.',
  // CTA
  app_boost: 'Tingkatkan tautan Anda hari ini',
  app_ctaStarted: 'Mulai Sekarang',

  // Footer
  app_footerFeatures: 'Fitur',
  app_link: 'Pemendekan Tautan',
  app_branded: 'Tautan Merek',
  app_analytics: 'Analitik',

  app_footerResources: 'Sumber Daya',
  app_blog: 'Blog',
  app_developers: 'Pengembang',
  app_support: 'Dukungan',
  app_company: 'Perusahaan',
  app_about: 'Tentang',
  app_team: 'Tim Kami',
  app_careers: 'Karir',
  app_contact: 'Kontak',

  // Error
  app_err1: 'Harap tambahkan URL',
  app_err2: 'URL tidak valid',
  app_err3: 'URL tidak diizinkan',
  app_err4: 'Terjadi kesalahan saat memendekkan URL.',
  error_11: 'Tautan Duplikat Terdeteksi!',
};
