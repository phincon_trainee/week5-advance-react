export default {
  app_greeting: 'Hi from Web!',
  app_title_header: 'Vite + React',
  app_not_found: 'Page not found',
  app_lang_id: 'Indonesian',
  app_lang_en: 'English',

  // header
  app_features: 'Features',
  app_pricing: 'Pricing',
  app_resources: 'Resources',
  app_login: 'Login',
  app_signup: 'Sign Up',

  // Page1
  app_More: 'More than just shorter links',
  app_Build: 'Build your brand`s recognition and get detailed insights on how your links are performing.',
  app_started: 'Get Started',

  // Shortener
  app_inputLink: 'Shorten a link here...',
  app_shorten: 'Shorten It!',
  app_clear: 'Clear',
  app_clearAll: 'Clear All',
  app_copy: 'Copy',
  app_copied: 'Copied',

  // Cards
  app_cardsTitle: 'Advanced Statistics',
  app_cardsChildText: 'Track how your links are performing across the web with our advanced statistics dashboard.',
  app_cardsTitle1: 'Brand Recognition',
  app_cardsChildText1:
    'Boost your brand recognition with each click. Generic links don`t means thing. Branded links help instil confidence in your content.',
  app_cardsTitle2: 'Detailed Records',
  app_cardsChildText2:
    'Gain insights into who is clicking your links. Knowing when and where people engage with your content helps inform better decisions.',
  app_cardsTitle3: 'Fully Customizable',
  app_cardsChildText3:
    'Improve brand awareness and content dicoverability through customizable links, supercharging audience engagement.',

  // CTA
  app_boost: 'Boost your link today',
  app_ctaStarted: 'Get Started',

  // Footer
  app_footerFeatures: 'Features',
  app_link: 'Link Shortening',
  app_branded: 'Branded Links',
  app_analytics: 'Analytics',

  app_footerResources: 'Resources',
  app_blog: 'Blog',
  app_developers: 'Developers',
  app_support: 'Support',

  app_company: 'Company',
  app_about: 'About',
  app_team: 'Our Team',
  app_careers: 'Careers',
  app_contact: 'Contact',

  // Error
  app_err1: 'Please add a URL',
  app_err2: 'Invalid URL',
  app_err3: 'URL is not allowed',
  app_err4: 'An error occurred while shortening the URL.',
  error_11: 'Duplicate URL Detected!',
};
