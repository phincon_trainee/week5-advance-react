/* eslint-disable no-unused-vars */
/* eslint-disable react/function-component-definition */
import React from 'react';
import { FormattedMessage } from 'react-intl';
import Brand from '@static/assets/icon-brand-recognition.svg';
import Records from '@static/assets/icon-detailed-records.svg';
import Customized from '@static/assets/icon-fully-customizable.svg';
import classes from './style.module.scss';
import Shortener from './shortener';

const CardData = [
  {
    image: Brand,
    title: <FormattedMessage id="app_cardsTitle1" />,
    info: <FormattedMessage id="app_cardsChildText1" />,
  },
  {
    image: Records,
    title: <FormattedMessage id="app_cardsTitle2" />,
    translate: 'translate-y-10',
    info: <FormattedMessage id="app_cardsChildText2" />,
  },
  {
    image: Customized,
    title: <FormattedMessage id="app_cardsTitle3" />,
    translate: 'translate-y-16',
    info: <FormattedMessage id="app_cardsChildText3" />,
  },
];

export default function Cards() {
  return (
    <div className={classes.backgroundCard}>
      <div className={classes.maincontainerCard}>
        <div className={classes.flexCard}>
          <div className={classes.gridCard}>
            <Shortener />
            <h2 className={classes.textCard}>
              <FormattedMessage id="app_cardsTitle" />
            </h2>

            <p className={classes.textChildCard}>
              <FormattedMessage id="app_cardsChildText" />
            </p>
          </div>
          <hr className={classes.line} />

          <div className={classes.cardData1}>
            {CardData.map((itemes, index) => {
              const { image, title, info, translate } = itemes;
              let cardClass;

              // Assign class based on the index
              if (index === 0) {
                cardClass = classes.card1;
              } else if (index === 1) {
                cardClass = classes.card2;
              } else if (index === 2) {
                cardClass = classes.card3;
              }

              return (
                <article className={`${classes.cardbox} ${translate} ${cardClass}`} key={index}>
                  <div className={classes.absolute}>
                    <img className={classes.icon} src={image} alt={`${title} Logo`} />
                  </div>
                  <span className={classes.titleCard}>{title}</span>
                  <p className={classes.infoCard}>{info}</p>
                </article>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
