/* eslint-disable no-alert */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-unused-vars */
/* eslint-disable react/function-component-definition */
import React, { useState, useEffect } from 'react';
import { useDispatch, connect } from 'react-redux';
import PropTypes from 'prop-types';
import bgImageDesktop from '@static/assets/bg-shorten-desktop.svg';
import bgImageMobile from '@static/assets/bg-shorten-mobile.svg';
import { getShortUrl, deleteShortUrl } from '@containers/App/actions';
import { selectShortUrl, selectShortUrlError, selectShortUrlLoadinng } from '@containers/App/selectors';
import { createStructuredSelector } from 'reselect';
import { injectIntl, FormattedMessage } from 'react-intl';
import Link from './Links';
import classes from './style.module.scss';

function Shortener({ shortURL1, shortUrlLoading, shortUrlError, intl }) {
  const dispatch = useDispatch();
  const [inputURL, setInputURL] = useState('');
  const [inputError, setInputError] = useState(false);
  const [resultURL, setResultURL] = useState(() => {
    const savedURL = localStorage.getItem(getShortUrl);
    if (savedURL) return JSON.parse(savedURL);
    return [];
  });

  useEffect(() => {
    // console.log(shortURL1);
    localStorage.setItem('shortURL', JSON.stringify(resultURL));
  }, [resultURL]);

  const [copied, setCopied] = useState(false);
  const handleCopy = (shortUrl) => {
    // Create a temporary textarea element to copy the text to the clipboard
    const tempTextarea = document.createElement('textarea');
    tempTextarea.value = shortUrl;
    document.body.appendChild(tempTextarea);

    // Select and copy the text
    tempTextarea.select();
    document.execCommand('copy');

    setCopied(true);

    // Reset the button text after a few seconds
    setTimeout(() => {
      setCopied(false);
    }, 2000); // You can adjust the timeout value as needed

    // Remove the temporary textarea element
    document.body.removeChild(tempTextarea);

    // You can show a notification or do any other action after copying.
    // For example:
    // alert('Short URL copied to clipboard!');
  };

  // const resultURL = useSelector(selectShortUrl);

  const [message, setMessage] = useState('');

  // Error message
  // console.log(shortUrlError, 'TEST');
  function getErrorMessage(errorCode) {
    switch (errorCode) {
      case 1:
        setMessage(<FormattedMessage id="app_err1" />);
        break;
      case 2:
        setMessage(<FormattedMessage id="app_err2" />);
        break;
      case 10:
        setMessage(<FormattedMessage id="app_err3" />);
        break;
      case 11:
        setMessage(<FormattedMessage id="error_11" />);
        break;
      default:
        // setMessage(<FormattedMessage id="app_err4" />);
        setMessage('');
    }
  }
  // console.log(shortUrlError);

  async function handleSubmit(e) {
    e.preventDefault();
    setInputError(false);
    const shortURL = await dispatch(getShortUrl(inputURL));
    shortUrlError ? getErrorMessage(shortUrlError.errorCode) : null;

    setInputURL(''); // Clear the input after submitting
    if (shortURL) {
      setResultURL([...resultURL, { original: inputURL, shortened: shortURL }]);
    }
  }
  const [setSelectedCode] = useState('');
  const handleClear = (savedURL) => {
    // localStorage.clear('shortURL');
    // setResultURL([]);
    const selectedCode = savedURL.code;
    dispatch(deleteShortUrl(selectedCode));
    setSelectedCode('');
  };

  useEffect(() => {
    shortUrlError ? getErrorMessage(shortUrlError.errorCode) : null;
  }, [shortUrlError]);

  return (
    <>
      <div className={classes.containerShort}>
        <div className={classes.relativeShort}>
          {/* <div className={classes.absoluteShort}> */}
          <picture>
            <source media="(min-width: 600px)" srcSet={bgImageDesktop} />
            <img
              className={classes.bgMobileShort}
              src={bgImageMobile}
              alt="CTA Background Blob"
              style={{ display: 'none' }}
            />
          </picture>
          {/* </div> */}
          <form onSubmit={handleSubmit} className={classes.isiCountainer}>
            <div className={classes.isi}>
              <input
                type="text"
                placeholder={intl.formatMessage({ id: 'app_inputLink' })}
                value={inputURL}
                className={`relative flex-1 rounded-md p-4 text-lg font-medium transition-all duration-300 ease-in-out lg:rounded-lg ${
                  inputError ? 'ring-4 ring-red-500 focus:ring-red-600' : 'focus:ring-4 focus:ring-cyan-500'
                }`}
                onChange={(e) => setInputURL(e.target.value)}
              />

              <p
                className={`absolute bottom-32 left-0 text-sm font-medium text-red-400 transition-all duration-300 ease-in-out sm:-bottom-7 md:-bottom-7 lg:-bottom-8 ${
                  inputError ? 'translate-y-0 opacity-100' : 'translate-y-1 opacity-0'
                }`}
              >
                {/* <i>Please add a link!!</i> */}
              </p>
            </div>
            <div className={classes.submitted}>
              {message ? <div className={classes.error}>{message}</div> : null}
              <button className={classes.submit} type="submit" disabled={shortUrlLoading}>
                {shortUrlLoading ? 'Loading...' : <FormattedMessage id="app_shorten" />}
              </button>
              {/* <button className={classes.clear} onClick={handleClear} type="button" disabled={resultURL.length === 0}>
                {resultURL.length > 1 ? <FormattedMessage id="app_clearAll" /> : <FormattedMessage id="app_clear" />}
              </button> */}
            </div>
          </form>
        </div>
      </div>
      {/* Display the result URLs */}
      <article className={classes.resultContainer}>
        {shortURL1.length > 0
          ? shortURL1.slice(-5).map((url, index) => (
              <div key={index} className={classes.resultItemWrapper}>
                <div key={index} className={classes.resultItem}>
                  <p className={classes.original} style={{ marginLeft: '10px' }}>
                    {url.original_link}
                  </p>
                  <hr className={classes.lineLink} />
                  <p className={classes.short}>{url.short_link}</p>
                  <div className={classes.btnLink}>
                    <button className={classes.copy} type="button" onClick={() => handleCopy(url.short_link)}>
                      {copied ? (
                        <FormattedMessage id="app_copied" className={classes.copied} />
                      ) : (
                        <FormattedMessage id="app_copy" />
                      )}
                    </button>
                    <button className={classes.clear} type="button" onClick={() => handleClear(url)}>
                      <FormattedMessage id="app_clear" />
                    </button>
                  </div>
                </div>
              </div>
            ))
          : null}
      </article>
    </>
  );
}
Shortener.propTypes = {
  shortURL1: PropTypes.arrayOf(
    PropTypes.shape({
      original_link: PropTypes.string,
      short_link: PropTypes.string,
    })
  ),
  shortUrlLoading: PropTypes.bool.isRequired,
  shortUrlError: PropTypes.object,
  intl: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  shortURL1: selectShortUrl,
  shortUrlLoading: selectShortUrlLoadinng,
  shortUrlError: selectShortUrlError,
});
export default injectIntl(connect(mapStateToProps)(Shortener));
