/* eslint-disable no-unused-vars */
/* eslint-disable react/button-has-type */
import React from 'react';
import bgImageDesktop from '@static/assets/bg-boost-desktop.svg';
import bgImageMobile from '@static/assets/bg-boost-mobile.svg';
import { FormattedMessage } from 'react-intl';
import classes from './style.module.scss';

// eslint-disable-next-line react/function-component-definition
export default function CTA() {
  return (
    <section className={classes.section}>
      <div className={classes.absoulte}>
        <picture>
          <source className={classes.bgImageDesktop} srcSet={bgImageDesktop} />
          <img className={classes.bgCTA} src={bgImageMobile} alt="CTA Background Blob" />
        </picture>
      </div>
      <div className={classes.mainContainerCTA}>
        <h2 className={classes.textCTA}>
          <FormattedMessage id="app_boost" />
        </h2>
        <button className={classes.startedCTA}>
          <FormattedMessage id="app_ctaStarted" />
        </button>
      </div>
    </section>
  );
}
