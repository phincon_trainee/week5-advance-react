/* eslint-disable react/button-has-type */
/* eslint-disable react/no-unescaped-entities */
// import { FormattedMessage } from 'react-intl';
import { FormattedMessage } from 'react-intl';
import HeroDesktop from '../../static/assets/illustration-working.svg';
import Cards from './cards';
import CTA from './CTA';
import classes from './style.module.scss';

const Home = () => (
  <div>
    <div className={classes.mainContainer}>
      <div className={classes.flex}>
        <div className={classes.grid}>
          <h1 className={classes.text}>
            <FormattedMessage id="app_More" />
          </h1>
          <p className={classes.textChild}>
            <FormattedMessage id="app_Build" />
          </p>

          <button className={classes.started}>
            <FormattedMessage id="app_started" />
          </button>
        </div>

        <div>
          <img className={classes.hero} src={HeroDesktop} alt="Main Hero Working Illustration" />
        </div>
      </div>
    </div>
    <Cards />
    <CTA />
  </div>
);

export default Home;
